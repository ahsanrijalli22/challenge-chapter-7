const express = require('express');
const router = express.Router();
const auth = require("../controllers/authController");
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get("/register", (req, res) => res.render("register"));
router.post("/register", auth.register);

// Login
router.get("/login", (req, res) => res.render("login"));
router.post("/login", auth.login);

// Register Page
router.post("/api/v1/auth/register", auth.register);

// Login Page
router.post("/api/v1/auth/login", auth.login);

// Middleware
const restrict = require("../middlewares/restrict");
router.get("/", restrict, (req, res) => res.render("index"));

router.get("/api/v1/auth/whoami", restrict, auth.whoami);

router.get("/whoami", restrict, auth.whoami);



module.exports = router;
